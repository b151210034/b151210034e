﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace b151210034_odev3.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult aile()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult korku()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult fantastik()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult gerilim()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult komedi()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult ChangeLanguage([Bind(Prefix = "lang")]string languageAbbreviation, string returnUrl)
        {
            if (languageAbbreviation != null)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageAbbreviation);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageAbbreviation);
            }
            HttpCookie cookie = new HttpCookie("Language");
            cookie.Value = languageAbbreviation;
            Response.Cookies.Add(cookie);

            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");

        }
    }
}