﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(b151210034_odev3.Startup))]
namespace b151210034_odev3
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
